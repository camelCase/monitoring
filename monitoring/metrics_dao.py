import json
import datetime
import tornado.gen as gen
from configuration import MonitoringConfiguration, DBConfig


class MetricsDAO(object):
    _saved_metrics_count = 0

    def __init__(self):
        object.__init__(self)
        self._motor_client = DBConfig.motor_client
        self._db = self._motor_client.balancer_database
        self._json_decoder = json.JSONDecoder()

    @gen.coroutine
    def save_metric(self, metric):
        metric_json = self._json_decoder.decode(metric)
        metric_json['datetime'] = datetime.datetime.strptime(
            metric_json['datetime'],
            MonitoringConfiguration.TIME_FORMAT
        )

        yield self._db.metrics.insert(metric_json)

        self._saved_metrics_count += 1

        if self._saved_metrics_count // 1000 > 0 \
           and (yield self._db.metrics.count()) > MonitoringConfiguration.MAX_STORED_METRICS_COUNT:
            self.remove_old_metrics()
            self._saved_metrics_count = 0

    @gen.coroutine
    def get_average_metrics_by_time_range(self, datetime_from, datetime_to, server_id, metrics_names):
        cursor = self._db.metrics.find(
            {
                'server_id': server_id,
                'datetime': {
                    '$lt': datetime_from,
                    '$gte': datetime_to
                }
            }
        )
        metrics = yield cursor.to_list(length=MonitoringConfiguration.MAX_METRICS_COUNT_TO_CALCULATE)
        indexes = []
        for i in range(1, len(metrics_names)):
            indexes.append(metrics[0]['metrics'].index_of(metrics_names[i]))

        avgs, counts = list(), list()
        for metric in metrics:
            for i in range(1, len(metrics_names)):
                value = metric['values'][indexes[i]]
                if value:
                    avgs[i] += metric['values'][indexes[i]]
                    counts[i] += 1

        for i in range(1, len(avgs)):
            avgs[i] /= counts[i]

    @gen.coroutine
    def remove_old_metrics(self):
        threshold = datetime.datetime.utcnow() - datetime.timedelta(hours=MonitoringConfiguration.METRICS_TTL_HOURS)
        yield self._db.metrics.remove(
            {
                'datetime': {
                    '$lt': threshold
                }
            }
        )
